// Part 1: fruitsOnSale

db.fruits.aggregate(
    [
        {
            $match: {
                onSale: true
            }
        },
        {
            $count: 'fruitsOnSale'
        }
    ]
);

// Part 2: enoughStock

db.fruits.aggregate(
    [
        {
            $match: {
                onSale: true,
                stock: {
                    $gte: 20
                }
            }
        },
        {
            $count: 'enoughStock'
        }
    ]
);

// Part 3: avg_price

db.fruits.aggregate(
    [
        {
            $match: {
                onSale: true,
            }
        },
        {
            $group: {
                _id: '$supplier_id',
                avg_price: {
                    $avg: '$price'
                }
            }
        }
    ]
);

// Part 4: max_price

db.fruits.aggregate(
    [
        {
            $match: {
                onSale: true,
            }
        },
        {
            $group: {
                _id: '$supplier_id',
                max_price: {
                    $max: '$price'
                }
            }
        },
        {
            $sort: {
                max_price: 1
            }
        }
    ]
);

// Part 5: min_price

db.fruits.aggregate(
    [
        {
            $match: {
                onSale: true,
            }
        },
        {
            $group: {
                _id: '$supplier_id',
                min_price: {
                    $min: '$price'
                }
            }
        },
        {
            $sort: {
                min_price: 1
            }
        }
    ]
);